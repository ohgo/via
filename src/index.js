'use strict';

const express = require('express');
const logger = require('./utilities/logger');
const controllers = require('./controllers');

const app = express();
app.use('/api', controllers);

if (!module.parent) app.listen(8000);

logger.debug('Listening to port 8000');

module.exports = app;
