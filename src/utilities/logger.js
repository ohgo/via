'use strict';

module.exports = {
  debug: console.log,
  info: console.info,
  warn: console.warn,
  error: console.error
};
