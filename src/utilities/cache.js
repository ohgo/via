'use strict';

const nodeCache = require('memory-cache');
const DEFAULT_TTL = 3600000; // 1 hour

function _put(key, value, ttl) {
  return nodeCache.put(key, value, ttl || DEFAULT_TTL)
}

module.exports = {
  set: _put,
  get: nodeCache.get
};
