'use strict';

const axios = require('axios');

function _get(url, options) {
  return axios.get(url, options)
    .then(response => { return response.data; })
    .catch(e => {
      throw new Error(e.response.config.method + ' request to ' + e.response.config.url + ' returned ' + e.response.status);
    });
}

module.exports = {
  get: _get
};
