'use strict';

const responseTime = require('response-time');
const logger = require('../utilities/logger');

function logRequest(req, res, time) {
  logger.info('%s request to %s returned %s in %d ms.', req.method, req.originalUrl, res.statusCode, time);
}

module.exports = responseTime(logRequest);
