'use strict';

const app = require('../index.js');
const request = require('supertest');
const expect = require('chai').expect;

describe('/trailer', function () {
  describe('GET endpoint', function () {
    it('given valid viaplay movie url, returns its youtube trailer url', () => {
      return request(app).get('/api/trailer?movieUrl=https://content.viaplay.se/pc-se/film/deadpool-2016')
        .expect(200)
        .then(response => {
          expect(response.body).to.be.an('object');
          expect(response.body.trailer).to.equal('https://www.youtube.com/watch?v=9vN6DHB6bJc');
        });
    });
    it('given other url, returns 500 with a standard message', () => {
      return request(app).get('/api/trailer?movieUrl=https://github.com/')
        .expect(500)
        .then(response => {
          expect(response.body).to.be.an('object');
          expect(response.body.message).to.be.a('string');
        });
    });
  });
});
