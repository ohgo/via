'use strict';

const router = require('express').Router();
const trailer = require('../models/trailer');

router.get('/', function (req, res, next) {
  const movieUrl = req.query.movieUrl;
  trailer.get(movieUrl)
    .then(link => res.json({
      trailer: link
    }))
    .catch(e => next(e));
});

module.exports = router;
