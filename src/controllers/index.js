'use strict';

const router = require('express').Router();
const trailerController = require('./trailer');
const requestLogger = require('../middlewares/request-logger');
const errorHandler = require('../middlewares/error-handler');

router.use(requestLogger);
router.use('/trailer', trailerController);
router.use(errorHandler);

module.exports = router;
