'use strict';

const sinon = require('sinon');
const sinonTestFactory = require('sinon-test');
const sinonTest = sinonTestFactory(sinon);
const expect = require('chai').expect;
const httpClient = require('../utilities/http-client');
const sut = require('./tmdb-video');

describe('tmdb-video model', function () {
  describe('getTrailerLink', function () {
    it('when a video resource is returned via http, returns its first youtube trailer FULL link', sinonTest(function () {
      const video = {
        'results': [{
          'key': 'teaser1',
          'site': 'YouTube',
          'type': 'Teaser'
        },
        {
          'key': 'trailer1',
          'site': 'YouTube',
          'type': 'Trailer'
        },
        {
          'key': 'trailer2',
          'site': 'YouTube',
          'type': 'Trailer'
        }]
      }

      this.stub(httpClient, 'get').resolves(video);

      return sut.getTrailerLink('an tmdb video link').then(trailerLink => {
        expect(trailerLink).to.equal('https://www.youtube.com/watch?v=trailer1');
      });

    }));
  });

});
