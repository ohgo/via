'use strict';

const sinon = require('sinon');
const sinonTestFactory = require('sinon-test');
const sinonTest = sinonTestFactory(sinon);
const expect = require('chai').expect;
const httpClient = require('../utilities/http-client');
const sut = require('./viaplay-movie');

describe('viaplay-movie model', function () {
  describe('getImdbId', function () {
    it('when a movie resource is returned via http, returns its imdb id', sinonTest(function () {
      const movie = {
        '_embedded': {
          'viaplay:blocks': [
            {
              '_embedded': {
                'viaplay:product': {
                  'content': {
                    'imdb': {
                      'id': 'the imdb id'
                    }
                  }
                }
              }
            }
          ]
        }
      };

      this.stub(httpClient, 'get').resolves(movie);

      return sut.getImdbId('a viaplay movie link').then(id => {
        expect(id).to.equal('the imdb id');
      });

    }));
  });

});
