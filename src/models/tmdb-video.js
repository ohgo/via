'use strict';

const httpClient = require('../utilities/http-client');
const tmdbApiKey = process.env.TMDB_API_KEY;

function _getTrailerLink(imdbId) {
  const url = 'https://api.themoviedb.org/3/movie/' + imdbId + '/videos';
  const options = {params: {external_source: 'imdb_id', api_key: tmdbApiKey}};

  return httpClient.get(url, options)
    .then(result => _parseTrailerLink(result))
}

function _parseTrailerLink(tmdbVideo) {
  const youtubeTrailer = tmdbVideo.results.find(r => _isYoutubeTrailer(r));
  return 'https://www.youtube.com/watch?v=' + youtubeTrailer.key;
}

function _isYoutubeTrailer(tmdbVideoResult) {
  // TMDB's docs doesn't say what other sites exist, so we support only youtube links for now.
  return tmdbVideoResult.site === 'YouTube' && tmdbVideoResult.type === 'Trailer';
}

module.exports = {
  getTrailerLink: _getTrailerLink
};
