'use strict';

const viaplayMovie = require('./viaplay-movie');
const tmdbVideo = require('./tmdb-video');
const cache = require('../utilities/cache');
const logger = require('../utilities/logger');

function _get(movieUrl) {
  const cachedLink = cache.get(movieUrl);
  if (cachedLink) return Promise.resolve(cachedLink);
  return _getTrailerLink(movieUrl).then(link => cache.set(movieUrl, link));
}

function _getTrailerLink(movieUrl) {
  logger.debug('Fetching trailer link externally.');
  return viaplayMovie.getImdbId(movieUrl)
    .then(imdbId => tmdbVideo.getTrailerLink(imdbId));
};

module.exports = {
  get: _get
};
