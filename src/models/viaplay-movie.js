'use strict';

const httpClient = require('../utilities/http-client');

function _getImdbId(movieUrl) {  
  return httpClient.get(movieUrl)
    .then(result => _parseImdbId(result));
}

function _parseImdbId(viaplayMovie) {
  return viaplayMovie._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.imdb.id;
}

module.exports = {
  getImdbId: _getImdbId
};
