# via
[![pipeline status](https://gitlab.com/ohgo/via/badges/master/pipeline.svg)](https://gitlab.com/ohgo/via/commits/master) [![coverage report](https://gitlab.com/ohgo/via/badges/master/coverage.svg)](https://gitlab.com/ohgo/via/commits/master)

API to take a viaplay movie link and return a link to its trailer

## Downstream dependencies

* [Viaplay Content API](https://content.viaplay.se/pc-se/film)
* [TMDB API](https://www.themoviedb.org/documentation/api)

## Running locally

* Make sure [Node](https://nodejs.org/en/) is installed
* [Get API key from TMDB](https://www.themoviedb.org/faq/api) and set it as **TMDB_API_KEY** environment variable

```sh
export TMDB_API_KEY=yourapikey
```

* In **/src**, install node dependencies then start the application

```sh
npm install
npm start
```

* Try it out! E.g. [Fast and Furious 8](http://localhost:8000/api/trailer?movieUrl=https://content.viaplay.se/pc-se/film/fast-and-furious-8-2017), [Get Out](http://localhost:8000/api/trailer?movieUrl=https://content.viaplay.se/pc-se/film/get-out-2017), [Deadpool](http://localhost:8000/api/trailer?movieUrl=https://content.viaplay.se/pc-se/film/deadpool-2016)

## Thoughts

### Cache

The in-memory cache should not be relied for heavy production load, since different nodes won't share the store. If all this API does is translating viaplay movie link into a trailer link, a simple & fast external key-value store like Redis is a good option.  
The cache, of course, does not have to be in the application (only). Depending on the larger context, other places can cache too: client, API gateway, web server, etc.

### Event-driven local copy

Depending on how business critical this function is, and if the number of reads (i.e. users) far exceeds writes (new movies, new trailers), a permanent local copy of trailer db may make sense. However, storing such copy typically does not comply with 3rd party API's terms of use. It will furthermore add significant complexity through use of events / pubsub pattern, so it should be carefully considered.

### Future code improvements

Network calls are unreliable, so the http client needs at least transient retries and perhaps a circuit breaker. The logger now is only from the global console object, but it is not too hard to replace it with real logger who can log a log-aggregator friendly format under different log levels. Furthermore, if this application is to be deployed, all configurations should be extracted (e.g. log level, TMDB's base url) so they can be configurable especially when deploying to multiple environments. The code could use more validations (Is the query string a valid url? Has Viaplay's Content API response model change?) and print more meaningful error messages. The code and especially the API should be documented so people know how to call them. I think I could go on a little longer, but I will stop here and close the case :)
